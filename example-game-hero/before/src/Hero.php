<?php

namespace SingleResponsibility\Example1\Before;

class Hero
{
    private $name;

    private $posX;
    private $posY;
    private $speed;

    private $attack;
    private $health;

    private $items;

    public function __construct(
        $name,
        $posX,
        $posY,
        $speed,
        $attack,
        $attackDistance,
        $health,
        array $items
    ) {
        $this->name             = $name;
        $this->posX             = $posX;
        $this->posY             = $posY;
        $this->speed            = $speed;
        $this->attack           = $attack;
        $this->attackDistance   = $attackDistance;
        $this->health           = $health;
        $this->items            = $items;
    }

    public function getName()
    {
        return $this->name;
    }

    public function salute()
    {
        return "Hello fellow Hero, my name is " . $this->getName();
    }

    public function taunt(Hero $target)
    {
        return "Do you have some last words $target->getName() ?";
    }

    public function getPosition()
    {
        return [
            $this->posX,
            $this->posY
        ];
    }

    public function move(array $newPosition)
    {
        $distance = $this->getDistance(
            $this->getPosition(),
            $newPosition
        );

        if ($distance > $this->getSpeed()) {
            return;
        }
        
        $this->posX = $newPosX;
        $this->posY = $newPosY;
    }

    public function getSpeed()
    {
        return $this->speed;
    }

    private function getDistance(
        array $iniPosition,
        array $endPosition
    ) {
        return sqrt(
            pow($endposition->getX() - $iniPosition->getX(), 2) +
            pow($endPosition->getY() - $iniPosition->getY(), 2)
        );
    }

    public function getAttack()
    {
        return $this->attack;
    }

    public function getHealth()
    {
        return $this->health;
    }

    public function setHealth($newHealth)
    {
        $this->health = $newHealth;
    }

    public function attack(Hero $target)
    {
        $distance = $this->getDistance(
            $this->getPosition(),
            $target->getPosition()
        );

        if ($distance > $this->getAttackDistance()) {
            return;
        }

        return $target->setHealth(
            $target->getHealth() - $this->getAttack()
        );
    }

    public function getItems()
    {
        return $this->items;
    }

    public function pickItem(Item $item)
    {
        $this->items[] = $item;
    }

    public function dropItem($index)
    {
        if ($index < 0 || $index >= count($this->items)) {
            return;
        }

        array_splice($this->items, $index);
    }

    public function useItem($index)
    {
        if ($index < 0 || $index >= count($this->items)) {
            return;
        }

        $selectedItem = $this->items[$index];
        
        switch ($selectedItem->getType()) {
            case "Health Potion":
                $this->health += 5;
                break;
            case "Rage Potion":
                $this->attack += 2;
                break;
        }

        $this->dropItem($index);
    }
}
