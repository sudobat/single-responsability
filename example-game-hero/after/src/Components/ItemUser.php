<?php

namespace SingleResponsibility\Example1\After\Components;

class ItemUser
{
    private $items;

    public function __construct($items)
    {
        $this->items = $items;
    }

    public function getItems()
    {
        return $this->items;
    }

    public function pickItem(Item $item)
    {
        $this->items[] = $item;
    }

    public function dropItem($index)
    {
        // if ($index < 0 || $index >= count($this->items)) {
        //     return;
        // }
        if (! $this->validIndex($index)) {
            return;
        }

        array_splice($this->items, $index);
    }

    public function useItem($index)
    {
        // if ($index < 0 || $index >= count($this->items)) {
        //     return;
        // }
        if (! $this->validIndex($index)) {
            return;
        }

        $selectedItem = $this->items[$index];
        
        // switch ($selectedItem->getType()) {
        //     case "Health Potion":
        //         $this->health += 5;
        //         break;
        //     case "Rage Potion":
        //         $this->attack += 2;
        //         break;
        // }
        $selectedItem->act($this->hero());

        $this->dropItem($index);
    }

    private function validIndex($index)
    {
        return $index < 0 || $index >= count($this->items);
    }
}
