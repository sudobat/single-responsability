<?php

namespace SingleResponsibility\Example1\After\Components;

class Fighter
{
    private $attack;
    private $attackDistance;
    private $health;

    public function getAttack()
    {
        return $this->attack;
        return $this->fighter->getAttack();
    }

    public function getHealth()
    {
        return $this->health;
        return $this->fighter->getHealth();
    }

    public function setHealth($newHealth)
    {
        $this->health = $newHealth;
    }

    public function attack(Hero $target)
    {
        $distance = $this->hero()
                         ->getPosition()
                         ->distanceTo($target->getPosition());

        if ($distance > $this->attackDistance) {
            return;
        }

        return $target->setHealth(
            $target->getHealth() - $this->getAttack()
        );
    }
}
