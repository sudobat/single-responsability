<?php

namespace SingleResponsibility\Example1\After\Components;

class Mover
{
    private $posX;
    private $posY;
    private $speed;

    public function __construct(
        $posX,
        $posY,
        $speed
    ) {
        $this->posX     = $posX;
        $this->posY     = $posY;
        $this->speed    = $speed;
    }

    public function getPosition()
    {
        return [
            $this->posX,
            $this->posY
        ];
    }

    public function move(Position $newPosition)
    {
        $distance = $this->getPosition()
                         ->distanceTo($newPosition);

        if ($distance > $this->getSpeed()) {
            return;
        }
        
        $this->posX = $newPosX;
        $this->posY = $newPosY;
    }

    public function getSpeed()
    {
        return $this->speed;
    }
}
