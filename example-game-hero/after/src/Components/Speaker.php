<?php

namespace SingleResponsibility\Example1\After\Components;

class Speaker
{
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function salute()
    {
        return "Hello fellow Hero, my name is " . $this->getName();
    }

    public function taunt(Hero $target)
    {
        return "Do you have some last words $target->getName() ?";
    }
}
