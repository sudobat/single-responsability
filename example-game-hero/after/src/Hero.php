<?php

namespace SingleResponsibility\Example1\After;

use SingleResponsibility\Example1\After\Components\Speaker;
use SingleResponsibility\Example1\After\Components\Attacker;
use SingleResponsibility\Example1\After\Components\Mover;
use SingleResponsibility\Example1\After\Components\ItemUser;

class Hero
{
    // private $name;

    // private $posX;
    // private $posY;
    // private $speed;

    // private $attack;
    // private $attackDistance;
    // private $health;

    // private $items;
    private $speaker;
    private $mover;
    private $fighter;
    private $itemUser;

    public function __construct(
        // $name,
        // $posX,
        // $posY,
        // $speed,
        // $attack,
        // $attackDistance,
        // $health,
        // array $items
        Speaker $speaker,
        Fighter $fighter,
        Mover $mover,
        ItemUser $itemUser
    ) {
        // $this->name             = $name;
        // $this->posX             = $posX;
        // $this->posY             = $posY;
        // $this->speed            = $speed;
        // $this->attack           = $attack;
        // $this->attackDistance   = $attackDistance;
        // $this->health           = $health;
        // $this->items            = $items;
        $this->speaker  = $speaker;
        $this->fighter  = $fighter;
        $this->mover    = $mover;
        $this->itemUser = $itemUser;
    }

    public function getName()
    {
        // return $this->name;
        return $this->speaker->getName();
    }

    public function salute()
    {
        // return "Hello fellow Hero, my name is " . $this->getName();
        return $this->speaker->salute();
    }

    public function taunt(Hero $target)
    {
        // return "Do you have some last words $target->getName() ?";
        return $this->speaker->taunt($target);
    }

    public function getPosition()
    {
        // return [
        //     $this->posX,
        //     $this->posY
        // ];
        return $this->mover->getPosition();
    }

    public function move(array $newPosition)
    {
        // $distance = $this->getDistance(
        //     $this->getPosition(),
        //     $newPosition
        // );

        // if ($distance > $this->getSpeed()) {
        //     return;
        // }
        
        // $this->posX = $newPosX;
        // $this->posY = $newPosY;
        return $this->mover->move($newPosition);
    }

    public function getSpeed()
    {
        // return $this->speed;
        return $this->mover->getSpeed();
    }

    // private function getDistance(
    //     array $iniPosition,
    //     array $endPosition
    // ) {
    //     return sqrt(
    //         pow($endposition->getX() - $iniPosition->getX(), 2) +
    //         pow($endPosition->getY() - $iniPosition->getY(), 2)
    //     );
    // }

    public function getAttack()
    {
        // return $this->attack;
        return $this->fighter->getAttack();
    }

    public function getHealth()
    {
        // return $this->health;
        return $this->fighter->getHealth();
    }

    public function setHealth($newHealth)
    {
        // $this->health = $newHealth;
        return $this->fighter->setHealth($newHealth);
    }

    public function attack(Hero $target)
    {
        // $distance = $this->getDistance(
        //     $this->getPosition(),
        //     $target->getPosition()
        // );

        // if ($distance > $this->getAttackDistance()) {
        //     return;
        // }

        // return $target->setHealth(
        //     $target->getHealth() - $this->getAttack()
        // );
        return $this->fighter->attack($target);
    }

    public function getItems()
    {
        // return $this->items;
        return $this->itemUser->getItems();
    }

    public function pickItem(Item $item)
    {
        // $this->items[] = $item;
        return $this->itemUser->pickItem($item);
    }

    public function dropItem($index)
    {
        // if ($index < 0 || $index >= count($this->items)) {
        //     return;
        // }

        // array_splice($this->items, $index);
        return $this->itemUser->dropItem($index);
    }

    public function useItem($index)
    {
        // if ($index < 0 || $index >= count($this->items)) {
        //     return;
        // }

        // $selectedItem = $this->items[$index];
        
        // switch ($selectedItem->getType()) {
        //     case "Health Potion":
        //         $this->health += 5;
        //         break;
        //     case "Rage Potion":
        //         $this->attack += 2;
        //         break;
        // }

        // $this->dropItem($index);
        return $this->itemUser->useItem($index);
    }
}
