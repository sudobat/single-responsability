<?php

namespace SingleResponsibility\ExampleFatModel\Before;

use MySQLStaticHandler;
use RedisStaticHandler;

class Product
{
    private $id;
    private $reference;
    private $price;
    private $name;
    private $stock;

    public function __construct(
        $id,
        $reference,
        $price,
        $name
    ) {
        $this->id           = $id;
        $this->reference    = $reference;
        $this->price        = $price;
        $this->name         = $name;
    }

    // Magic getter
    public function __get($property)
    {
        return $this->$property;
    }

    public function setReference($newReference)
    {
        if (! $this->isValidReference($newPrice)) {
            return;
        }

        $this->price = $newPrice;
    }

    public function setPrice($newPrice)
    {
        if (! $this->isValidPrice($newPrice)) {
            return;
        }

        $this->price = $newPrice;
    }

    public function setName($newName)
    {
        if (! $this->isValidName($newPrice)) {
            return;
        }

        $this->name = $newName;
    }

    // public function getFromMySQLById($productId)
    // {
    //     $sql = "
    //         SELECT * FROM products
    //         WHERE productPK = ?
    //     ";

    //     $mysqlResult = MySQLStaticHandler::exec($sql, $productId);

    //     $product = $mysqlResult->fetch();

    //     return $product;
    // }

    // public static function getFromMySQLByCategoryId($categoryId)
    // {
    //      $sql = "
    //         SELECT * FROM products
    //         WHERE product_FK_category_PK = ?
    //     ";

    //     $mysqlResult = MySQLStaticHandler::exec($sql, $categoryId);

    //     $product = $mysqlResult->fetch();

    //     return $product;
    // }

    // public static function getFromRedisById($productId)
    // {
    //     return RedisStaticHandler::get('product', $productId);
    // }

    // public static function updateToMySQL()
    // {
    //     $sql = "
    //         UPDATE products SET 
    //         product_FK_category_PK = ?,
    //         reference = ?,
    //         name = ?
    //         WHERE productPK = ?
    //     ";

    //     MySQLStaticHandler::exec($sql, [
    //         $this->categoryId,
    //         $this->reference,
    //         $this->name,
    //         $this->id,
    //     ]);
    // }

    // public static function deleteFromMySQLById($productId)
    // {
    //     $sql = "
    //         DELETE FROM products
    //         WHERE productPK = ?
    //     ";

    //     MySQLStaticHandler::exec($sql, $productId);
    // }

    // public static function deleteFromRedisById($productId)
    // {
    //     RedisStaticHandler::delete('product', $productId);
    // }
}
