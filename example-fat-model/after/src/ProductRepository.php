<?php

namespace SingleResponsibility\ExampleFatModel\Before;

use Product;
use Infrastructure\Databases\MySQLDatabase;
use Infrastructure\Databases\RedisDatabase;

class ProductRepository
{
    public function __construct(
        MySQLDatabase $mySQLDatabase,
        RedisDatabase $redisDatabase
    ) {
        $this->mySQLDatabase    = $mySQLDatabase;
        $this->redisDatabase    = $redisDatabase;
    }

    public function getById($productId)
    {
        $productArray = $this->redisDatabase->get('product', $productId);

        if ($productArray != null) {
            $product = new Product(
                $productArray['id'],
                $productArray['reference'],
                $productArray['price'],
                $productArray['name'],
                $productArray['stock']
            );

            return $product;
        }

        $sql = "
            SELECT * FROM products
            WHERE productPK = ?
        ";

        $mysqlResult = $mySQLDatabase->exec($sql, $productId);

        $productArray = $mysqlResult->fetch();

        if ($productArray == null) {
            return null;
        }

        $product = new Product(
            $productArray['id'],
            $productArray['reference'],
            $productArray['price'],
            $productArray['name'],
            $productArray['stock']
        );

        return $product;
    }

    public static function getFromMySQLByCategoryId($categoryId)
    {
         $sql = "
            SELECT * FROM products
            WHERE product_FK_category_PK = ?
        ";

        $mysqlResult = MySQLStaticHandler::exec($sql, $categoryId);

        $product = $mysqlResult->fetch();

        return $product;
    }

    public static function getFromRedisById($productId)
    {
        return RedisStaticHandler::get('product', $productId);
    }

    public static function updateToMySQL()
    {
        $sql = "
            UPDATE products SET 
            product_FK_category_PK = ?,
            reference = ?,
            name = ?
            WHERE productPK = ?
        ";

        MySQLStaticHandler::exec($sql, [
            $this->categoryId,
            $this->reference,
            $this->name,
            $this->id,
        ]);
    }

    public static function deleteFromMySQLById($productId)
    {
        $sql = "
            DELETE FROM products
            WHERE productPK = ?
        ";

        MySQLStaticHandler::exec($sql, $productId);
    }

    public static function deleteFromRedisById($productId)
    {
        RedisStaticHandler::delete('product', $productId);
    }
}
